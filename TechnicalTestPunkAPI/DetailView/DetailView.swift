//
//  DetailView.swift
//  TechnicalTestPunkAPI
//
//  Created by Francisco José Navarro García on 02/03/2020.
//  Copyright © 2020 AtalayaSoft OÜ. All rights reserved.
//

import Foundation
import UIKit

class DetailView: UIViewController {

    // MARK: Properties
    var presenter: DetailPresenterProtocol?

    @IBOutlet var imageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var volumeLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    
    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter?.viewDidLoad()
    }
}

extension DetailView: DetailViewProtocol {
    // TODO: implement view output methods
    
    func showDetailBeer(_ beer: Beer) {
        if let imageURL = beer.imageURL {
            imageView.sd_setImage(with: URL(string: imageURL),
                                  completed: nil)
        }
        
        if let name = beer.name {
            nameLabel.text = name
        }
        
        if let volume = beer.volume,
            let unit = volume.unit,
            let value = volume.value {
            volumeLabel.text = "\(value) \(unit)"
        }
        
        if let description = beer.beerDescription {
            descriptionLabel.text = description
        }
    }
}
