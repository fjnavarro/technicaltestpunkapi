//
//  HomeView.swift
//  TechnicalTestPunkAPI
//
//  Created by Francisco José Navarro García on 01/03/2020.
//  Copyright © 2020 AtalayaSoft OÜ. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class HomeView: UIViewController {
    
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var listCollectionView: UICollectionView!
    @IBOutlet var activityIndicatorView: UIActivityIndicatorView!
    
    // MARK: Properties
    var presenter: HomePresenterProtocol?
    var beers: Beers?

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension HomeView: HomeViewProtocol {
    // TODO: implement view output methods
    func updateBeers(_ beers: Beers) {
        self.beers = beers
        listCollectionView.reloadData()
    }
    
    func loadActivity() {
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
        }
    }
    
    func stopActivity() {
        DispatchQueue.main.async {
            self.activityIndicatorView.stopAnimating()
        }
    }
}

extension HomeView: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter?.searchText(searchText)
    }
}

extension HomeView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return beers?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellIdentifier", for: indexPath)
        
        if let cell = cell as? HomeCollectionViewCell,
            let beers = beers {
            let beer = beers[indexPath.row]
            
            if let imageURL = beer.imageURL {
                cell.image.sd_setImage(with: URL(string: imageURL),
                                       completed: nil)
            }
            
            if let name = beer.name {
                cell.titleLabel.text = name
            }
        }
        
        return cell
    }
}

extension HomeView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let beers = beers {
            let beer = beers[indexPath.row]
            
            presenter?.showDetail(beer)
        }
    }
}
