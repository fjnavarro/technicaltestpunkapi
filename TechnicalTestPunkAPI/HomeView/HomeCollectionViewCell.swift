//
//  HomeCollectionViewCell.swift
//  TechnicalTestPunkAPI
//
//  Created by Francisco José Navarro García on 02/03/2020.
//  Copyright © 2020 AtalayaSoft OÜ. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var image: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    
}
