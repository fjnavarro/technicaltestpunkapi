//
//  HomePresenter.swift
//  TechnicalTestPunkAPI
//
//  Created by Francisco José Navarro García on 01/03/2020.
//  Copyright © 2020 AtalayaSoft OÜ. All rights reserved.
//

import Foundation

class HomePresenter  {
    
    // MARK: Properties
    weak var view: HomeViewProtocol?
    var interactor: HomeInteractorInputProtocol?
    var wireFrame: HomeWireFrameProtocol?
    
}

extension HomePresenter: HomePresenterProtocol {
    // TODO: implement presenter methods
    func viewDidLoad() {
    }
    
    func searchText(_ searchText: String) {
        view?.loadActivity()
        
        interactor?.getBeers(with: searchText)
    }
    
    func showDetail(_ beer: Beer) {
        if let view = view {
            wireFrame?.showDetail(beer,
                                  from: view)
        }
    }
}

extension HomePresenter: HomeInteractorOutputProtocol {
    // TODO: implement interactor output methods
    func callBackGetBeers(with beers: Beers) {
        view?.stopActivity()
        
        view?.updateBeers(beers)
    }
}
