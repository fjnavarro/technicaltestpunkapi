//
//  HomeRemoteDataManager.swift
//  TechnicalTestPunkAPI
//
//  Created by Francisco José Navarro García on 01/03/2020.
//  Copyright © 2020 AtalayaSoft OÜ. All rights reserved.
//

import Foundation
import Alamofire

class HomeRemoteDataManager:HomeRemoteDataManagerInputProtocol {
    
    var remoteRequestHandler: HomeRemoteDataManagerOutputProtocol?
    
    func getBeers(with food: String) {
        let request = AF.request("https://api.punkapi.com/v2/beers?food=\(food)")
        
        request.responseDecodable(of: Beers.self) { (response) in
            guard let beers = response.value else {
                self.remoteRequestHandler?.callBackGetBeers(with: Beers())
                
                return
            }
            
            self.remoteRequestHandler?.callBackGetBeers(with: beers)
        }
    }
}
