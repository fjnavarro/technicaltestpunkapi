# TechnicalTestPunkAPI - iPhone App

I have used the VIPER architecture.

# Installation guide

### Step 0: Clone this repo ;P

### Step 1: Install Cocoa Pods

This project uses Cocoa Pods for the management and installation of packages, so if it is not installed, it must be installed with the following command:

```sh
sudo gem install cocoapods
```

### Step 2: Install packages or pods

To install the Pods we must go to the root of the project and execute the following command:

```sh
$ pod install
```

### Step 3: Open the Workspace

To open the project you must use the .xcworkspace file and not the .xcodeproj (so the cocoa pods libraries will be loaded)

```sh
open TechnicalTestPunkAPI.xcworkspace
```
